<?php
/**
 * Quote Builder plugin for Craft CMS 3.x
 *
 * Build html quotes from CMS
 *
 * @link      https://kurious.agency
 * @copyright Copyright (c) 2019 Kurious Agency
 */

namespace kuriousagency\quotebuilder\controllers;

use kuriousagency\quotebuilder\QuoteBuilder;

use Craft;
use craft\web\Controller;

/**
 * Quotes Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Kurious Agency
 * @package   QuoteBuilder
 * @since     1.0.0
 */
class QuotesController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'do-something'];

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our plugin's index action URL,
     * e.g.: actions/quote-builder/quotes
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $variables['quotes'] = QuoteBuilder::$plugin->quotes->getAllQuotes();

        return $this->renderTemplate('quote-builder/quotes/index', $variables);
    }

    /**
     * Handle a request going to our plugin's actionDoSomething URL,
     * e.g.: actions/quote-builder/quotes/do-something
     *
     * @return mixed
     */
    public function actionEdit($id = null)
    {
        $variables['quote'] = QuoteBuilder::$plugin->quotes->getQuoteById($id);

        return $this->renderTemplate('quote-builder/quotes/edit', $variables);
    }
}
