<?php
/**
 * Quote Builder plugin for Craft CMS 3.x
 *
 * Build html quotes from CMS
 *
 * @link      https://kurious.agency
 * @copyright Copyright (c) 2019 Kurious Agency
 */

namespace kuriousagency\quotebuilder\services;

use kuriousagency\quotebuilder\QuoteBuilder;
use kuriousagency\quotebuilder\models\Quote;
use kuriousagency\quotebuilder\models\Product;
use kuriousagency\quotebuilder\models\ProductType;
use kuriousagency\quotebuilder\models\Staff;
use kuriousagency\quotebuilder\models\Customer;
use kuriousagency\quotebuilder\records\Quote as QuoteRecord;
use kuriousagency\quotebuilder\records\Product as ProductRecord;
use kuriousagency\quotebuilder\records\ProductType as ProductTypeRecord;
use kuriousagency\quotebuilder\records\Staff as StaffRecord;
use kuriousagency\quotebuilder\records\Customer as CustomerRecord;

use Craft;
use craft\base\Component;
use craft\db\Query;


/**
 * Quotes Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Kurious Agency
 * @package   QuoteBuilder
 * @since     1.0.0
 */
class Quotes extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     QuoteBuilder::$plugin->quotes->exampleService()
     *
     * @return mixed
     */
    public function getQuoteById($id)
    {
        $result = QuoteRecord::findOne($id);
        return $result;
    }

    public function getAllQuotes()
    {
        $result = QuoteRecord::find()
            ->orderBy('dateUpdated')
            ->all();
        return $result;
    }

    // Product Related Services

    public function getProductById($id)
    {
        $result = ProductRecord::findOne($id);
        return $result;
    }

    public function getAllProducts()
    {
        $result = ProductRecord::find()
            ->orderBy('id')
            ->all();
        return $result;
    }

    public function saveProduct(Product $model)
    {
        $isNewType = !$model->id;
        if ($model->id) {
            $record = ProductRecord::findOne($model->id);
            if (!$record) {
                throw new Exception(Craft::t('quote-builder', 'No Product with the ID "{id}" exists',['id' => $model->id]));
            }
        } else {
            $record = new ProductRecord();
        }
        $record->name = $model->name;
        $record->productTypeId = $model->productTypeId;
        $record->save(false);

        if ($isNewType) {
            $model->id = $record->id;
        }

        return true;
    }

    public function deleteProductById($id = null)
    {
        $product = ProductRecord::findOne($id);
        if ($product) {
            return $product->delete();
        }
        return false;
    }

    // Product Type Related Services
    public function getProductTypeById($id)
    {
        $result = ProductTypeRecord::findOne($id);
        return $result;
    }

    public function getAllProductTypes()
    {
        $result = ProductTypeRecord::find()
            ->orderBy('id')
            ->all();
        return $result;
    }

    public function saveProductType(ProductType $model)
    {
        $isNewType = !$model->id;
        if ($model->id) {
            $record = ProductTypeRecord::findOne($model->id);
            if (!$record) {
                throw new Exception(Craft::t('quote-builder', 'No Product Type with the ID "{id}" exists',['id' => $model->id]));
            }
        } else {
            $record = new ProductTypeRecord();
        }
        $record->name = $model->name;
        $record->hourprice = $model->hourprice;
        $record->fixedPrice = $model->fixedPrice;
        $record->save(false);

        if ($isNewType) {
            $model->id = $record->id;
        }

        return true;
    }

    public function deleteTypeById($id = null)
    {
        $type = ProductTypeRecord::findOne($id);
        if ($type) {
            return $type->delete();
        }
        return false;
    }

    // Staff Related Services

    public function getStaffById($id)
    {
        $result = StaffRecord::findOne($id);
        return $result;
    }
    public function getStaffByEmail($email)
    {
        $query = (new Query())
            ->select(['staff.email'])
            ->from(['{{%quotebuilder_staff}} staff'])
            ->where(['email'=>$email])
            ->one();
        return $query;
    }

    public function getAllStaff()
    {
        $result = StaffRecord::find()
            ->orderBy('id')
            ->all();
        return $result;
    }

    public function saveStaff(Staff $model)
    {
        $isNewStaff = !$model->id;
        if ($model->id) {
            $record = StaffRecord::findOne($model->id);
            if (!$record) {
                throw new Exception(Craft::t('quote-builder', 'No Team member with the ID "{id}" exists',['id' => $model->id]));
            }
        } else {
            $record = new StaffRecord();
            if ($this->getStaffByEmail($model->email)) {
                return Craft::t('quote-builder', 'Email "{email}" already associated with Team Member',['email' => $model->email]);
            }
        }
        $record->name = $model->name;
        $record->email = $model->email;
        $record->save(false);

        if ($isNewStaff) {
            $model->id = $record->id;
        }

        return true;
    }

    public function deleteStaffById($id = null)
    {
        $staff = StaffRecord::findOne($id);
        if ($staff) {
            return $staff->delete();
        }
        return false;
    }

    // Customer Related Services

    public function getCustomerById($id)
    {
        $result = CustomerRecord::findOne($id);
        return $result;
    }

    public function getCustomerByEmail($email)
    {
        $query = (new Query())
            ->select(['customer.email'])
            ->from(['{{%quotebuilder_customer}} customer'])
            ->where(['email'=>$email])
            ->one();
        return $query;
    }

    public function getAllCustomers()
    {
        $result = CustomerRecord::find()
            ->orderBy('id')
            ->all();
        return $result;
    }

    public function saveCustomer(Customer $model)
    {
        $isNewCustomer = !$model->id;
        if ($model->id) {
            $record = CustomerRecord::findOne($model->id);
            if (!$record) {
                throw new Exception(Craft::t('quote-builder', 'No Customer with the ID "{id}" exists',['id' => $model->id]));
            }
        } else {
            $record = new CustomerRecord();
            if ($this->getCustomerByEmail($model->email)) {
                return Craft::t('quote-builder', 'Email "{email}" already associated with a Customer',['email' => $model->email]);
            }
        }
        $record->name = $model->name;
        $record->email = $model->email;
        $record->company = $model->company;
        $record->companyAddress = $model->companyAddress;
        $record->save(false);

        if ($isNewCustomer) {
            $model->id = $record->id;
        }

        return true;
    }

    public function deleteCustomerById($id = null)
    {
        $customer = CustomerRecord::findOne($id);
        if ($customer) {
            return $customer->delete();
        }
        return false;
    }
}
