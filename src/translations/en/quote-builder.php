<?php
/**
 * Quote Builder plugin for Craft CMS 3.x
 *
 * Build html quotes from CMS
 *
 * @link      https://kurious.agency
 * @copyright Copyright (c) 2019 Kurious Agency
 */

/**
 * Quote Builder en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('quote-builder', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Kurious Agency
 * @package   QuoteBuilder
 * @since     1.0.0
 */
return [
    'Quote Builder plugin loaded' => 'Quote Builder plugin loaded',
];
