/**
 * Quote Builder plugin for Craft CMS
 *
 * Quote Builder JS
 *
 * @author    Kurious Agency
 * @copyright Copyright (c) 2019 Kurious Agency
 * @link      https://kurious.agency
 * @package   QuoteBuilder
 * @since     1.0.0
 */
