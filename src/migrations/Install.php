<?php
/**
 * Quote Builder plugin for Craft CMS 3.x
 *
 * Build html quotes from CMS
 *
 * @link      https://kurious.agency
 * @copyright Copyright (c) 2019 Kurious Agency
 */

namespace kuriousagency\quotebuilder\migrations;

use kuriousagency\quotebuilder\QuoteBuilder;

use Craft;
use craft\config\DbConfig;
use craft\db\Migration;

/**
 * Quote Builder Install Migration
 *
 * If your plugin needs to create any custom database tables when it gets installed,
 * create a migrations/ folder within your plugin folder, and save an Install.php file
 * within it using the following template:
 *
 * If you need to perform any additional actions on install/uninstall, override the
 * safeUp() and safeDown() methods.
 *
 * @author    Kurious Agency
 * @package   QuoteBuilder
 * @since     1.0.0
 */
class Install extends Migration
{
    // Public Properties
    // =========================================================================

    /**
     * @var string The database driver to use
     */
    public $driver;

    // Public Methods
    // =========================================================================

    /**
     * This method contains the logic to be executed when applying this migration.
     * This method differs from [[up()]] in that the DB logic implemented here will
     * be enclosed within a DB transaction.
     * Child classes may implement this method instead of [[up()]] if the DB logic
     * needs to be within a transaction.
     *
     * @return boolean return a false value to indicate the migration fails
     * and should not proceed further. All other return values mean the migration succeeds.
     */
    public function safeUp()
    {
        $this->driver = Craft::$app->getConfig()->getDb()->driver;
        if ($this->createTables()) {
            $this->createIndexes();
            $this->addForeignKeys();
            // Refresh the db schema caches
            Craft::$app->db->schema->refresh();
            $this->insertDefaultData();
        }

        return true;
    }

    /**
     * This method contains the logic to be executed when removing this migration.
     * This method differs from [[down()]] in that the DB logic implemented here will
     * be enclosed within a DB transaction.
     * Child classes may implement this method instead of [[down()]] if the DB logic
     * needs to be within a transaction.
     *
     * @return boolean return a false value to indicate the migration fails
     * and should not proceed further. All other return values mean the migration succeeds.
     */
    public function safeDown()
    {
        $this->driver = Craft::$app->getConfig()->getDb()->driver;
        $this->removeTables();

        return true;
    }

    // Protected Methods
    // =========================================================================

    /**
     * Creates the tables needed for the Records used by the plugin
     *
     * @return bool
     */
    protected function createTables()
    {
        $tablesCreated = false;

    // quotebuilder_quote table
        $tableSchema = Craft::$app->db->schema->getTableSchema('{{%quotebuilder_quote}}');
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                '{{%quotebuilder_quote}}',
                [
                    'id' => $this->primaryKey(),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                // Custom columns in the table
                    'siteId' => $this->integer()->notNull()->defaultValue(Craft::$app->sites->currentSite->id),
                    'quoterId' => $this->integer(),
                    'customerId' => $this->integer()->notNull(),
                    'summary' => $this->string(),
                    'products' => $this->string(),
                    'subtotal' => $this->decimal(14,2),
                    'discount' => $this->decimal(14,2),
                    'discountedTotal' => $this->decimal(14,2),
                    'tax' => $this->decimal(14,2),
                    'total' => $this->decimal(14,2),
                    'currency' => $this->string()
                ]
            );
        }

    // quotebuilder_product table
        $tableSchema = Craft::$app->db->schema->getTableSchema('{{%quotebuilder_product}}');
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                '{{%quotebuilder_product}}',
                [
                    'id' => $this->primaryKey(),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                // Custom columns in the table
                    'siteId' => $this->integer()->notNull()->defaultValue(Craft::$app->sites->currentSite->id),
                    'name' => $this->string(255)->notNull()->defaultValue(''),
                    'ref' => $this->string(),
                    'productTypeId' => $this->integer()->notNull()
                ]
            );
        }

    // quotebuilder_staff table
        $tableSchema = Craft::$app->db->schema->getTableSchema('{{%quotebuilder_staff}}');
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                '{{%quotebuilder_staff}}',
                [
                    'id' => $this->primaryKey(),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                // Custom columns in the table
                    'siteId' => $this->integer()->notNull()->defaultValue(Craft::$app->sites->currentSite->id),
                    'name' => $this->string(255)->notNull()->defaultValue(''),
                    'email' => $this->string(),
                    'assetId' => $this->integer()
                ]
            );
        }

    // quotebuilder_customer table
        $tableSchema = Craft::$app->db->schema->getTableSchema('{{%quotebuilder_customer}}');
        if ($tableSchema === null) {
            $tablesCreated = true;
            $this->createTable(
                '{{%quotebuilder_customer}}',
                [
                    'id' => $this->primaryKey(),
                    'dateCreated' => $this->dateTime()->notNull(),
                    'dateUpdated' => $this->dateTime()->notNull(),
                    'uid' => $this->uid(),
                // Custom columns in the table
                    'siteId' => $this->integer()->notNull()->defaultValue(Craft::$app->sites->currentSite->id),
                    'name' => $this->string(255)->notNull()->defaultValue(''),
                    'email' => $this->string(),
                    'company' => $this->string(),
                    'companyAddress' =>$this->string()
                ]
            );
        }

    // quotebuilder_productType table
    $tableSchema = Craft::$app->db->schema->getTableSchema('{{%quotebuilder_productType}}');
    if ($tableSchema === null) {
        $tablesCreated = true;
        $this->createTable(
            '{{%quotebuilder_productType}}',
            [
                'id' => $this->primaryKey(),
                'dateCreated' => $this->dateTime()->notNull(),
                'dateUpdated' => $this->dateTime()->notNull(),
                'uid' => $this->uid(),
            // Custom columns in the table
                'siteId' => $this->integer()->notNull()->defaultValue(Craft::$app->sites->currentSite->id),
                'name' => $this->string(255)->notNull()->defaultValue(''),
                'hourprice' => $this->decimal(14,2),
                'fixedPrice' => $this->boolean()
            ]
        );
    }

        return $tablesCreated;
    }

    /**
     * Creates the indexes needed for the Records used by the plugin
     *
     * @return void
     */
    protected function createIndexes()
    {
    // quotebuilder_quote table

        $this->createIndex(
            $this->db->getIndexName(
                '{{%quotebuilder_quote}}',
                'id',
                true
            ),
            '{{%quotebuilder_quote}}',
            'id',
            true
        );
        // Additional commands depending on the db driver
        switch ($this->driver) {
            case DbConfig::DRIVER_MYSQL:
                break;
            case DbConfig::DRIVER_PGSQL:
                break;
        }

    // quotebuilder_product table
        
        $this->createIndex(
            $this->db->getIndexName(
                '{{%quotebuilder_product}}',
                'id',
                true
            ),
            '{{%quotebuilder_product}}',
            'id',
            true
        );

        $this->createIndex(
            $this->db->getIndexName(
                '{{%quotebuilder_product}}',
                'ref',
                true
            ),
            '{{%quotebuilder_product}}',
            'ref',
            true
        );
        // Additional commands depending on the db driver
        switch ($this->driver) {
            case DbConfig::DRIVER_MYSQL:
                break;
            case DbConfig::DRIVER_PGSQL:
                break;
        }

    // quotebuilder_staff table
        $this->createIndex(
            $this->db->getIndexName(
                '{{%quotebuilder_staff}}',
                'id',
                true
            ),
            '{{%quotebuilder_staff}}',
            'id',
            true
        );
        // Additional commands depending on the db driver
        switch ($this->driver) {
            case DbConfig::DRIVER_MYSQL:
                break;
            case DbConfig::DRIVER_PGSQL:
                break;
        }

    // quotebuilder_customer table
        $this->createIndex(
            $this->db->getIndexName(
                '{{%quotebuilder_customer}}',
                'id',
                true
            ),
            '{{%quotebuilder_customer}}',
            'id',
            true
        );
        // Additional commands depending on the db driver
        switch ($this->driver) {
            case DbConfig::DRIVER_MYSQL:
                break;
            case DbConfig::DRIVER_PGSQL:
                break;
        }
    }

    /**
     * Creates the foreign keys needed for the Records used by the plugin
     *
     * @return void
     */
    protected function addForeignKeys()
    {
    // quotebuilder_quote table
    $this->addForeignKey(null,'{{%quotebuilder_quote}}','siteId','{{%sites}}','id','CASCADE','CASCADE');
    $this->addForeignKey(null,'{{%quotebuilder_quote}}','quoterId','{{%quotebuilder_staff}}','id','CASCADE','CASCADE');
    $this->addForeignKey(null,'{{%quotebuilder_quote}}','customerId','{{%quotebuilder_customer}}','id','CASCADE','CASCADE');
    // quotebuilder_product table
    $this->addForeignKey(null,'{{%quotebuilder_product}}','siteId','{{%sites}}','id','CASCADE','CASCADE');
    $this->addForeignKey(null,'{{%quotebuilder_product}}','productTypeId','{{%quotebuilder_productType}}','id','CASCADE','CASCADE');
    // quotebuilder_staff table
    $this->addForeignKey(null,'{{%quotebuilder_staff}}','siteId','{{%sites}}','id','CASCADE','CASCADE');
    $this->addForeignKey(null,'{{%quotebuilder_staff}}','assetId','{{%assets}}','id','CASCADE','CASCADE');
    // quotebuilder_customer table
    $this->addForeignKey(null,'{{%quotebuilder_customer}}','siteId','{{%sites}}','id','CASCADE','CASCADE');
    // quotebuilder_productType table
    $this->addForeignKey(null,'{{%quotebuilder_productType}}','siteId','{{%sites}}','id','CASCADE','CASCADE');
    }

    /**
     * Populates the DB with the default data.
     *
     * @return void
     */
    protected function insertDefaultData()
    {
    }

    /**
     * Removes the tables needed for the Records used by the plugin
     *
     * @return void
     */
    protected function removeTables()
    {
    // quotebuilder_quote table
        $this->dropTableIfExists('{{%quotebuilder_quote}}');

    // quotebuilder_product table
        $this->dropTableIfExists('{{%quotebuilder_product}}');

    // quotebuilder_staff table
        $this->dropTableIfExists('{{%quotebuilder_staff}}');

    // quotebuilder_customer table
        $this->dropTableIfExists('{{%quotebuilder_customer}}');
    
    // quotebuilder_productType table
        $this->dropTableIfExists('{{%quotebuilder_productType}}');
    }
}
