<?php
/**
 * Quote Builder plugin for Craft CMS 3.x
 *
 * Build html quotes from CMS
 *
 * @link      https://kurious.agency
 * @copyright Copyright (c) 2019 Kurious Agency
 */

namespace kuriousagency\quotebuilder;

use kuriousagency\quotebuilder\services\Quotes as QuotesService;
use kuriousagency\quotebuilder\models\Settings;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\web\UrlManager;
use craft\events\RegisterUrlRulesEvent;

use yii\base\Event;

/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 *
 * @author    Kurious Agency
 * @package   QuoteBuilder
 * @since     1.0.0
 *
 * @property  QuotesService $quotes
 * @property  Settings $settings
 * @method    Settings getSettings()
 */
class QuoteBuilder extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * Static property that is an instance of this plugin class so that it can be accessed via
     * QuoteBuilder::$plugin
     *
     * @var QuoteBuilder
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * To execute your plugin’s migrations, you’ll need to increase its schema version.
     *
     * @var string
     */
    public $schemaVersion = '1.0.0';

    // Public Methods
    // =========================================================================

    /**
     * Set our $plugin static property to this class so that it can be accessed via
     * QuoteBuilder::$plugin
     *
     * Called after the plugin class is instantiated; do any one-time initialization
     * here such as hooks and events.
     *
     * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
     * you do not need to load it in your init() method.
     *
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        // Register our site routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['siteActionTrigger1'] = 'quote-builder/quotes';
            }
        );

        // Register our CP routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_CP_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                // Quotes
                $event->rules['quote-builder'] = 'quote-builder/quotes/index';
                $event->rules['quote-builder/quotes'] = 'quote-builder/quotes/index';
                $event->rules['quote-builder/quotes/edit'] = 'quote-builder/quotes/edit';
                $event->rules['quote-builder/quotes/edit/<id:\d+>'] = 'quote-builder/quotes/edit';
                $event->rules['quote-builder/quotes/view/<id:\d+>'] = 'quote-builder/quotes/view';
                // Products
                $event->rules['quote-builder/products'] = 'quote-builder/products/index';
                $event->rules['quote-builder/products/edit'] = 'quote-builder/products/edit';
                $event->rules['quote-builder/products/edit/<id:\d+>'] = 'quote-builder/products/edit';
                //Product Types
                $event->rules['quote-builder/product-types'] = 'quote-builder/product-types/index';
                $event->rules['quote-builder/product-types/edit'] = 'quote-builder/product-types/edit';
                $event->rules['quote-builder/product-types/edit/<id:\d+>'] = 'quote-builder/product-types/edit';
                //Staff
                $event->rules['quote-builder/staff'] = 'quote-builder/staff/index';
                $event->rules['quote-builder/staff/edit'] = 'quote-builder/staff/edit';
                $event->rules['quote-builder/staff/edit/<id:\d+>'] = 'quote-builder/staff/edit';
                //Customers
                $event->rules['quote-builder/customers'] = 'quote-builder/customers/index';
                $event->rules['quote-builder/customers/edit'] = 'quote-builder/customers/edit';
                $event->rules['quote-builder/customers/edit/<id:\d+>'] = 'quote-builder/customers/edit';

            }
        );

        // Do something after we're installed
        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                    // We were just installed
                }
            }
        );

        

/**
 * Logging in Craft involves using one of the following methods:
 *
 * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
 * Craft::info(): record a message that conveys some useful information.
 * Craft::warning(): record a warning message that indicates something unexpected has happened.
 * Craft::error(): record a fatal error that should be investigated as soon as possible.
 *
 * Unless `devMode` is on, only Craft::warning() & Craft::error() will log to `craft/storage/logs/web.log`
 *
 * It's recommended that you pass in the magic constant `__METHOD__` as the second parameter, which sets
 * the category to the method (prefixed with the fully qualified class name) where the constant appears.
 *
 * To enable the Yii debug toolbar, go to your user account in the AdminCP and check the
 * [] Show the debug toolbar on the front end & [] Show the debug toolbar on the Control Panel
 *
 * http://www.yiiframework.com/doc-2.0/guide-runtime-logging.html
 */
        Craft::info(
            Craft::t(
                'quote-builder',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    public function getCpNavItem(): array
    {
        $item = parent::getCpNavItem();
        $item['subnav']['quotes'] = ['label' => 'Quotes', 'url' => 'quote-builder'];
        $item['subnav']['customers'] = ['label' => 'Customers', 'url' => 'quote-builder/customers/'];
        $item['subnav']['staff'] = ['label' => 'Team', 'url' => 'quote-builder/staff'];
        $item['subnav']['products'] = ['label' => 'Products', 'url' => 'quote-builder/products'];
        $item['subnav']['productTypes'] = ['label' => 'Product Types', 'url' => 'quote-builder/product-types'];
  
        return $item;
    }

    // Protected Methods
    // =========================================================================

    /**
     * Creates and returns the model used to store the plugin’s settings.
     *
     * @return \craft\base\Model|null
     */
    protected function createSettingsModel()
    {
        return new Settings();
    }

    /**
     * Returns the rendered settings HTML, which will be inserted into the content
     * block on the settings page.
     *
     * @return string The rendered settings HTML
     */
    protected function settingsHtml(): string
    {
        return Craft::$app->view->renderTemplate(
            'quote-builder/settings',
            [
                'settings' => $this->getSettings()
            ]
        );
    }
}
